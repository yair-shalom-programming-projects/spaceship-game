
public class Runner extends RobotSpaceShip {

    Runner() { super(); }
    public static SpaceShip createShip() { return new Runner(); }


    @Override
    public void doAction(SpaceWars game) {

        super.doAction(game);

        _physics.move(true, 0);

        SpaceShip other = game.getClosestShipTo(this);

        if ( _physics.distanceFrom(other.getPhysics()) <= 0.25 )
            if ( _physics.angleTo(other.getPhysics()) <= 0.23 )
                teleport();

        incEnergy();
    }

}
