import oop.ex2.*;

public class RobotSpaceShip extends SpaceShip {


    RobotSpaceShip() {
        super();
        _image = GameGUI.ENEMY_SPACESHIP_IMAGE;
    }

    @Override
    public void shieldOn() {

        if (_curEnergy >= 3) {

            _image = GameGUI.ENEMY_SPACESHIP_IMAGE_SHIELD;
            _curEnergy -= 3;
        }
    }

    @Override
    public boolean isShieldOn() {
        return _image == GameGUI.ENEMY_SPACESHIP_IMAGE_SHIELD; }

    @Override
    public void closeShield() { _image = GameGUI.ENEMY_SPACESHIP_IMAGE; }


}
