import oop.ex2.SpaceShipPhysics;

public class Special extends RobotSpaceShip {

    public Special() { super(); }
    public static SpaceShip createShip() { return new Special(); }


    @Override
    public void doAction( SpaceWars game ) {

        super.doAction(game);

        SpaceShipPhysics other = game.getClosestShipTo(this).getPhysics();

        //  move to closer to ship
        if ( _physics.angleTo(other) != 0 )
            _physics.move(true, _physics.angleTo(other) > 0 ? 1 : -1);


        if ( _physics.distanceFrom(other) <= 0.19 ) {
            shieldOn();
            if ( _physics.angleTo(other) <= 0.19 )
                fire(game);

        }

        // inc energy
        incEnergy();
    }


}
