import java.util.Random;

public class Drunked extends RobotSpaceShip {

    Drunked() { super(); }

    public static SpaceShip createShip() { return new Drunked(); }

    @Override
    public void doAction(SpaceWars game) {

        super.doAction(game);
        Random rnd = new Random();

        // Unexpected behavior

        // 1. teleport
        if ( rnd.nextBoolean() )
            teleport();

        // 2. move
        _physics.move(rnd.nextBoolean(), rnd.nextInt(2));

        // 3. open shield
        if ( rnd.nextBoolean() )
            shieldOn();

        // 4. fire a shot
        if ( rnd.nextBoolean() )
            fire(game);

        // 5. inc current energy
        incEnergy();

    }

}
