public class Basher extends RobotSpaceShip{

    Basher() { super(); }

    public static SpaceShip createShip() {
        return new Basher();
    }


    @Override
    public void doAction(SpaceWars game)
    {
        super.doAction(game);

        _physics.move(true, 0);

        SpaceShip other = game.getClosestShipTo(this);

        if ( _physics.distanceFrom(other.getPhysics()) <= 0.19 )
                shieldOn();

        incEnergy();
    }
}
