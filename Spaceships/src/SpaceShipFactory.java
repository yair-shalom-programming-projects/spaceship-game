import oop.ex2.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

public class SpaceShipFactory {
    public static SpaceShip[] createSpaceShips(String[] args) {

        Map <String, Supplier<SpaceShip>> ships_map = new HashMap<>(){{
            put("h", Human::createShip); put("d", Drunked::createShip);
            put("r", Runner::createShip); put("a", Aggressive::createShip);
            put("b", Basher::createShip); put("s", Special::createShip);
        }};

        List<SpaceShip> ships = new ArrayList<SpaceShip>();

        for (int i = 0; i < args.length; i++)

            ships.add(ships_map.get(args[i]).get());


        return ships.toArray(SpaceShip[]::new);

    }
}
