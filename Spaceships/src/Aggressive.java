import oop.ex2.SpaceShipPhysics;

public class Aggressive extends RobotSpaceShip {

    Aggressive() { super(); }

    public static SpaceShip createShip() { return new Aggressive(); }


    @Override
    public void doAction(SpaceWars game) {

        super.doAction(game);

        // accel. always.
        _physics.move(true, 0);

        SpaceShip other = game.getClosestShipTo(this);
        SpaceShipPhysics otherPhysics = other.getPhysics();

        // move to closer to ship
        if (_physics.angleTo(otherPhysics) != 0)
            _physics.move(true, _physics.angleTo(otherPhysics) > 0 ? 1 : -1);

        // try shoot
        if ( _physics.distanceFrom(otherPhysics) <= 0.19)
            fire(game);


        incEnergy();

    }
}
