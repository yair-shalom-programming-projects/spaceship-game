import java.awt.Image;
import oop.ex2.*;

/**
 * The API spaceships need to implement for the SpaceWars game. 
 * It is your decision whether SpaceShip.java will be an interface, an abstract class,
 *  a base class for the other spaceships or any other option you will choose.
 *  
 * @author oop
 */
public class SpaceShip {

    protected java.awt.Image _image;
    protected SpaceShipPhysics _physics;
    protected int _maxEnergy;
    protected int _curEnergy;
    protected int _hearts;
    protected int _countSevenTurns;


    SpaceShip(){
        reset();
    }

    /**
     * Does the actions of this ship for this round. 
     * This is called once per round by the SpaceWars game driver.
     * 
     * @param game the game object to which this ship belongs.
     */
    public void doAction(SpaceWars game) {
        if ( isShieldOn() ) closeShield();
        if ( _countSevenTurns == 0 ) _countSevenTurns++;
    }


    /**
     * This method is called every time a collision with this ship occurs 
     */
    public void collidedWithAnotherShip() {
        if ( isShieldOn() ) {
            _curEnergy += 18;
            _maxEnergy += 18;
        }
        else {
            _maxEnergy -= 10;
            _hearts--;
        }
    }


    /** 
     * This method is called whenever a ship has died. It resets the ship's 
     * attributes, and starts it at a new random position.
     */
    public void reset() {

        _physics = new SpaceShipPhysics();
        _maxEnergy = 210;
        _curEnergy = 190;
        _hearts = 22;

        _countSevenTurns = -1;
    }

    /**
     * Checks if this ship is dead.
     * 
     * @return true if the ship is dead. false otherwise.
     */
    public boolean isDead() {
        return _hearts == 0;
    }


    /**
     * Gets the physics object that controls this ship.
     * 
     * @return the physics object that controls the ship.
     */
    public SpaceShipPhysics getPhysics() {
        return _physics;
    }

    /**
     * This method is called by the SpaceWars game object when ever this ship
     * gets hit by a shot.
     */
    public void gotHit() {

        if ( !isShieldOn() ){

            _maxEnergy -= 10;
            _hearts--;
        }
    }

    /**
     * Gets the image of this ship. This method should return the image of the
     * ship with or without the shield. This will be displayed on the GUI at
     * the end of the round.
     * 
     * @return the image of this ship.
     */
    public Image getImage() {
        return _image;
    }

    /**
     * Attempts to fire a shot.
     * 
     * @param game the game object.
     * @return is a shot was fire on not
     */
    public void fire(SpaceWars game) {

        if ( _curEnergy >= 19 && (_countSevenTurns == -1 || _countSevenTurns == 7) ) {

            game.addShot(_physics);
            _curEnergy -= 19;

            _countSevenTurns = 0;
        }
    }

    /**
     * Attempts to turn on the shield.
     */
    public void shieldOn() {
    }

    /**
     * Attempts to teleport.
     */
    public void teleport() {

       if (_curEnergy >= 140) {
           _physics = new SpaceShipPhysics();
           _curEnergy -= 140;
       }
    }

    public boolean isShieldOn() { return  false; }
    public void closeShield() {}

    public void incEnergy() {
        if ( _curEnergy < _maxEnergy )
            _curEnergy++;
    };

}
