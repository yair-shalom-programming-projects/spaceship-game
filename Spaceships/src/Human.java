import oop.ex2.*;


public class Human extends SpaceShip {

    Human() {
        super();
        _image = GameGUI.SPACESHIP_IMAGE;
    }

    // return new human ship //
    public static SpaceShip createShip() {
        return new Human();
    }

    @Override
    public void doAction(SpaceWars game) {

        super.doAction(game);


        // 1. Teleport
        if ( game.getGUI().isTeleportPressed() )
            teleport();

        // 2. move & acceleration
        if ( game.getGUI().isLeftPressed() )
            _physics.move(false, 1);
        else if( game.getGUI().isRightPressed() )
            _physics.move(false, -1);
        else if( game.getGUI().isUpPressed() )
            _physics.move(true, 0);

        // 3. Shield
        if ( game.getGUI().isShieldsPressed() )
            shieldOn();

        // 4. Shot
        if ( game.getGUI().isShotPressed())
            fire(game);

        // 5. inc energy
        incEnergy();

    }
    @Override
    public void shieldOn() {

        if (_curEnergy >= 3) {

            _image = GameGUI.SPACESHIP_IMAGE_SHIELD;
            _curEnergy -= 3;
        }
    }
    @Override
    public boolean isShieldOn() {
        return _image == GameGUI.SPACESHIP_IMAGE_SHIELD; }

    @Override
    public void closeShield() { _image = GameGUI.SPACESHIP_IMAGE; }
}
